
package sysbook;

import cliente.Cliente;
import Telas.JFramePrincipal;
import emprestimo.Emprestimo;
import livros.Autor;
import livros.Livro;
import genero.Genero;
import java.util.ArrayList;
import java.util.List;


public class SysBook {

    
    public static List <Cliente> listCliente = new ArrayList<>();
    public static List <Emprestimo> ListEmprestimo = new ArrayList<>();
    public static List <Autor> ListAutor = new ArrayList<>();
    public static List <Livro> ListLivro = new ArrayList<>();
    public static List <Genero> ListGenero = new ArrayList<>();
    public static List <JFramePrincipal> ListJFramePrincipal = new ArrayList<>();

    public static List<Cliente> getListCliente() {
        return listCliente;
    }

    public static void setListCliente(List<Cliente> listCliente) {
        SysBook.listCliente = listCliente;
    }

    public static List<Emprestimo> getListEmprestimo() {
        return ListEmprestimo;
    }

    public static void setListEmprestimo(List<Emprestimo> ListEmprestimo) {
        SysBook.ListEmprestimo = ListEmprestimo;
    }

    public static List<Autor> getListAutor() {
        return ListAutor;
    }

    public static void setListAutor(List<Autor> ListAutor) {
        SysBook.ListAutor = ListAutor;
    }

    public static List<Livro> getListLivro() {
        return ListLivro;
    }

    public static void setListLivro(List<Livro> ListLivro) {
        SysBook.ListLivro = ListLivro;
    }

    public static List<Genero> getListGenero() {
        return ListGenero;
    }

    public static void setListGenero(List<Genero> ListGenero) {
        SysBook.ListGenero = ListGenero;
    }

    public static List<JFramePrincipal> getListJFramePrincipal() {
        return ListJFramePrincipal;
    }

    public static void setListJFramePrincipal(List<JFramePrincipal> ListJFramePrincipal) {
        SysBook.ListJFramePrincipal = ListJFramePrincipal;
    }
    
    
    public static void main(String[] args) {
        new JFramePrincipal();
        
    }
    //livro
    public static void addLivro(Livro livro) {
        ListLivro.add (livro);
    }

    public static void addLivro(String livro) {
    }

   

    //emprestimo
    public static void addEmprestimo(Emprestimo emprestimo) {
        ListEmprestimo.add (emprestimo);
    }
    
    //autor
    public static void addAutor(Autor autor) {
        ListAutor.add(autor);
    }

    public static void addAutor(String autor) {
    }

    

    
  
}
